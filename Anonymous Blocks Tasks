Task 1: Write an anonymous block that declares a variable v_greeting and initializes it with "Hello, World!". The block should then output the value of v_greeting.

DECLARE
  v_greeting VARCHAR2(50) := 'Hello, World!';
BEGIN
  DBMS_OUTPUT.PUT_LINE(v_greeting);
END;
/

Task 2: Declare two number variables, v_num1 and v_num2. Initialize v_num1 with 10. Within the block, assign v_num2 the value of v_num1 multiplied by 5 and then output it.

DECLARE
    v_num1 NUMBER := 10;
    v_num2 NUMBER;
BEGIN
    v_num2 := v_num1 * 5;
    dbms_output.put_line(v_num2);
END;

/

Task 3: Declare two variables, v_length and v_width, and initialize them with 5 and 10 respectively. Calculate the area of a rectangle using these and output the result.

DECLARE
    v_length NUMBER := 5;
    v_width  NUMBER := 10;
    v_area   NUMBER;
BEGIN
    v_area := v_length * v_width;
    dbms_output.put_line('Area of rectangle: ' || v_area);
END;

/

Task 4: Declare a date variable v_today and initialize it with the current date. Add 7 days to this date and output the result.

DECLARE
    v_today DATE := sysdate;
    v_date  DATE;
BEGIN
    v_date := v_today + 7;
    dbms_output.put_line('7 days adding to v_today: ' || v_date);
END;

/

Task 5: Declare two string variables, v_firstName and v_lastName. Initialize them with your first and last name. Concatenate them to display a full name and then output it.

DECLARE
    v_firstname VARCHAR2(50) := 'Nilufar';
    v_lastname  VARCHAR2(50) := 'Mammadova';
BEGIN
    dbms_output.put_line('Full Name: '
                         || v_firstname
                         || ' '
                         || v_lastname);
END;

/

Task 6: Declare a number variable v_age. If v_age is greater than 18, print "Adult". Otherwise, print "Minor".

DECLARE
    v_age NUMBER := 17;
BEGIN
    IF v_age > 18 THEN
        dbms_output.put_line('Adult');
    ELSE
        dbms_output.put_line('Minor');
    END IF;
END;

/

Task 7: Declare a number variable v_counter and initialize it with 1. Write a loop that outputs the numbers from 1 to 5 using this variable.

DECLARE
    v_counter NUMBER := 1;
BEGIN
    WHILE v_counter <= 5 LOOP
        dbms_output.put_line(v_counter);
        v_counter := v_counter + 1;
    END LOOP;
END;

/

Task 8: Declare a number variable v_dividend and v_divisor. Initialize v_divisor with 0. Try dividing v_dividend by v_divisor and handle any exceptions to print "Cannot divide by zero".

DECLARE
    v_dividend NUMBER;
    v_divisor  NUMBER := 0;
    v_result   NUMBER;
BEGIN
    v_result := v_dividend / v_divisor;
    dbms_output.put_line('Result: ' || v_result);
EXCEPTION
    WHEN zero_divide THEN
        dbms_output.put_line('Cannot divide by zero');
END;

/

Task 9: Declare a string variable v_text and initialize it with "oracle". Convert the string to uppercase and then output the result.

DECLARE
    v_text VARCHAR2(30) := 'oracle';
BEGIN
    dbms_output.put_line(upper(v_text));
END;

/

Task 10: Write an anonymous block that declares a number variable v_outerVar and initializes it with 100. Within this block, declare another nested anonymous block that declares a variable v_innerVar and initializes it with 200. Output both variables from the inner block.

DECLARE
    v_outervar NUMBER;
BEGIN
    DECLARE
        v_innervar NUMBER;
    BEGIN
        v_outervar := 100;
        v_innervar := 200;
        dbms_output.put_line('Inner variable: '
                             || v_innervar
                             || ' '
                             || 'and'
                             || ' '
                             || 'Outer variable: '
                             || v_outervar);

    END;
END;

/
